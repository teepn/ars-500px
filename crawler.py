import csv
import json
import os
import requests
import sqlite3
import sys

from requests_oauthlib import OAuth1

CLIENT_KEY = '3iAQcIZ7SPTLKmVNeVdYjKGVNAX3qry1IqBmO3Uf'
CLIENT_SECRET = '7kZ0QdPKogzfevliru0XuMNNRWG6cz1NcTzurJWI'
FIRST_ID = '17000481'
# id_user_list = ['10599609'] # Old id not working followers
DB_NAME = 'data.csv'
ID_USER_LIST_FILE = 'id_user_list.json'
FIELDNAMES = ['id', 'username', 'firstname', 'lastname', 'birthday', 'sex', 'city', 'state', 'country',
              'about', 'friends_count', 'followers_count', ]
MAX_ITEM_PAGE = 100
N_ITEM_TO_CRAWL = 10


def call(id, call_type, connection, page=1):
    if call_type == "get_info":
        url = 'https://api.500px.com/v1/users/show?id=' + str(id)
        r = requests.get(url, auth=connection)

    elif call_type == "get_following":
        url = 'https://api.500px.com/v1/users/' + str(id) + '/friends/?page=' + str(page) + '&rpp=' + str(MAX_ITEM_PAGE)

    elif call_type == "get_follower":
        url = 'https://api.500px.com/v1/users/' + str(id) + '/followers/?page=' + str(page) + '&rpp=' + str(
            MAX_ITEM_PAGE)

    r = requests.get(url, auth=connection)
    return json.loads(r.text, "utf-8")










# Open connection with db
con_db = sqlite3.connect('data.sqlite')


with con_db:
    cur = con_db.cursor()
    # Clear db if -a is not in arg
    if "-a" not in sys.argv:
        # Clear db and insert the element 0
        cur.execute('DELETE FROM users_500px')
        con_db.commit()

        cur.execute('DELETE FROM edge_users_500px')
        con_db.commit()

        # Insert the element 0
        cur.execute('INSERT INTO users_500px ("id", "is_crawled") VALUES ("' + FIRST_ID + '", "0")')
        con_db.commit()
        item_inserted = 1

        users_crawled = 0
    elif "-a" in sys.argv:
        cur.execute('SELECT count(*) FROM users_500px WHERE is_crawled =1')
        users_count = cur.fetchone()
        users_crawled = users_count[0]

        cur.execute('SELECT count(*) FROM users_500px')
        users_count = cur.fetchone()
        item_inserted = users_count[0]




# Open connection with 500px
con_500px = OAuth1(client_key=CLIENT_KEY, client_secret=CLIENT_SECRET)


while (users_crawled < N_ITEM_TO_CRAWL):

    # Get all element not crawled
    with con_db:
        cur = con_db.cursor()
        cur.execute('SELECT id, is_crawled FROM users_500px WHERE is_crawled =0')
        users_550px = cur.fetchall()

    user = users_550px[0]
    print "=================="
    print "USER: %d" % user[0]
    print "... Retrive info"

    # =====================Get info
    info_call = call(user[0], "get_info", con_500px)

    with con_db:
        cur = con_db.cursor()
        cur.execute("UPDATE users_500px SET username=?, firstname=?, lastname=?, birthday=?, "
                    "sex=?, city=?, state=?, country=?, about=?, friends_count=?, followers_count=? "
                    "WHERE id=?",
                    (info_call['user']['username'],
                     info_call['user']['firstname'],
                     info_call['user']['lastname'],
                     info_call['user']['birthday'],
                     info_call['user']['sex'],
                     info_call['user']['city'],
                     info_call['user']['state'],
                     info_call['user']['country'],
                     info_call['user']['about'].replace('\r\n', ' '),
                     info_call['user']['friends_count'],
                     info_call['user']['followers_count'],
                     user[0]))
        con_db.commit()
        print "... Got info"

        # =============================Get following
        if (info_call['user']['friends_count'] > 0):
            print "... Try to get %d following" % info_call['user']['friends_count']
            pages_count = ((info_call['user']['friends_count'] // MAX_ITEM_PAGE) + 1)
            # if (pages_count > 3): pages_count = 3
            friend_list = ""
            friends_count=0
            for current_page in range(1, pages_count + 1):
                #if item_inserted == N_ITEM_TO_CRAWL: break
                print "...  ... Call following: Page %d of %s" % (current_page, pages_count)

                try:
                    following_call = call(user[0], "get_following", con_500px, page=current_page)
                    # For each friend
                    for friend in following_call['friends']:


                        #Insert current friend and current user in relation table
                        with con_db:
                            cur = con_db.cursor()
                            cur.execute('PRAGMA foreign_keys = ON')
                            #try to insert element
                            try:
                                cur.execute('INSERT INTO edge_users_500px ("source", "destination") '
                                            'VALUES ("' + str(user[0]) + '", "'+ str(friend['id']) +'")')

                                con_db.commit()
                            except sqlite3.IntegrityError:

                                pass


                            friends_count += 1
                        # Insert item in db only if the number of id in db is < count item to crawl
                        if item_inserted < N_ITEM_TO_CRAWL:
                            # Try to insert id in db, pass if insert produced an error caused to duplicate id
                            try:
                                cur.execute('INSERT INTO users_500px ("id", "is_crawled") '
                                            'VALUES ("' + str(friend['id']) + '", "0")')
                                con_db.commit()
                                item_inserted += 1
                            except sqlite3.IntegrityError:
                                print "Following gia' presente"
                                pass


                except ValueError:
                    print "Errore in chiamata"
                    current_page = pages_count + 1

            print "... Got following"

        # =============================Get follower
        if info_call['user']['followers_count'] > 0:

            #Make first call for get pages_count
            try:
                followers_call = call(user[0], "get_follower", con_500px)
                pages = followers_call['followers_pages']



                #print "... Try to get %d follower" % info_call['user']['followers_count']

                for current_page in range(1, pages + 1):
                    #if item_inserted == N_ITEM_TO_CRAWL: break
                    print "...  ... Call follower: Page %d of %s" % (current_page, pages)


                    followers_call = call(user[0], "get_follower", con_500px, page=current_page)

                    # For each follower
                    for follower in followers_call['followers']:

                        # Insert current friend and current user in relation table
                        with con_db:
                            cur = con_db.cursor()
                            cur.execute('PRAGMA foreign_keys = ON')
                            # try to insert element

                            try:
                                cur.execute('INSERT INTO edge_users_500px ("source", "destination") '
                                            'VALUES ("' + str(follower['id']) + '", "' + str(user[0]) + '")')


                                con_db.commit()
                            except sqlite3.IntegrityError:

                                pass

                        # Insert item in db only if the number of id in db is < count item to crawl
                        if item_inserted < N_ITEM_TO_CRAWL:


                            # Try to insert id in db pass if insert produced an error caused to duplicate id
                            try:
                                cur.execute(
                                    'INSERT INTO users_500px ("id", "is_crawled") VALUES ("' + str(
                                        follower['id']) + '", "0")')
                                con_db.commit()
                                item_inserted += 1
                            except sqlite3.IntegrityError:
                                print "Follower gia' presente"
                                pass

            except ValueError:
                #Exit from for
                print "Errore in chiamata"


            print "... Got follower"




        # Get count element crawled
        # Update current element as crawled
        cur.execute("UPDATE users_500px SET is_crawled=? WHERE id=?", ("1", user[0]))
        con_db.commit()
        cur.execute('SELECT count(*) FROM users_500px WHERE is_crawled =1')
        users_count = cur.fetchone()
        users_crawled = users_count[0]


        print "**** %d element crawled ****" % users_crawled




"""
TODO Modificare get in e out in modo che prenda il count e le pagine direttamente dalla risposta della chiamata (count utente fallace)
TODO Modifica script in modo che generi automaticamente il csv
"""

#
