import csv
import json
import os
import requests
import sqlite3
import sys
import random

from requests.packages.urllib3.exceptions import HTTPError
from requests_oauthlib import OAuth1

# Api Key 500px
CLIENT_KEY = 'l26mealUPxeEJcXmy1uRFC4BoXDWw3WLZAATHEEM'
CLIENT_SECRET = 'CtwhIBsZ7ezKp5jDs3PtGBP1B7H9BhJMbCP0k31L'

# File name of sqlite db
DB_NAME = 'data2.sqlite'

# Field names of get info
FIELDNAMES = ['id', 'username', 'firstname', 'lastname', 'birthday', 'sex', 'city', 'state', 'country',
              'about', 'friends_count', 'followers_count', ]

MAX_ITEM_PAGE = 100
N_ITEM_TO_CRAWL = 8000



# Open connection with db
con_db = sqlite3.connect(DB_NAME)


with con_db:
    cur = con_db.cursor()

    # Clear db if -a is not in arg
    if "-a" not in sys.argv:
        # Clear db and insert the element 0
        cur.execute('DELETE FROM nodes')
        con_db.commit()

        cur.execute('DELETE FROM link')
        con_db.commit()

        i=1
        uid = 3
        # uid = 6271985  # Error 404
        # uid = 10599609 # Error timeout
        # uid = 107941   # Error 107941
        uid_list = []

    elif "-a" in sys.argv:

        #Get node inserted
        cur.execute('SELECT count(*) FROM nodes')
        n = cur.fetchone()
        node_count = n[0]

        #Get all element
        cur.execute('SELECT id FROM nodes')
        nodes = cur.fetchall()


        if N_ITEM_TO_CRAWL - node_count > 0:
            i = N_ITEM_TO_CRAWL - node_count

            cur.execute('SELECT id FROM nodes ORDER BY id DESC')
            last = cur.fetchone()
            uid= last[0]
            print "Riprendo a scaricare i nodi da "+str(uid)

        elif N_ITEM_TO_CRAWL - node_count == 0:
            i = N_ITEM_TO_CRAWL +1
            print "Riprendo a costruire le relazioni "

        uid_list =[node[0] for node in nodes]





    con_api = OAuth1(client_key=CLIENT_KEY, client_secret=CLIENT_SECRET)



    # ************ NODES **************
    print "******* NODES *******"

    #for i in range(1, N_ITEM_TO_CRAWL+1):
    while (i <= N_ITEM_TO_CRAWL):

        flag_repeat=True
        while flag_repeat:
            print "%d / %d --> uid: %d" % (i, N_ITEM_TO_CRAWL, uid)

            # Prova a chiamare se va a buon fine esci dal ciclo altrimenti incrementa uid e ripova
            try:
                url = 'https://api.500px.com/v1/users/show?id=' + str(uid)
                r = requests.get(url, auth=con_api, timeout=3)
                if r.status_code // 100 == 2:
                    get_info_call = json.loads(r.text, "utf-8")
                    flag_repeat = False
                else:
                    uid += 1

            except requests.RequestException as e: uid+=1


        if get_info_call['user']['about']: get_info_call['user']['about'] = get_info_call['user']['about'].replace('\r\n', ' ')
        try:
            cur.execute("INSERT INTO nodes VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        (get_info_call['user']['id'], get_info_call['user']['username'], get_info_call['user']['firstname'],
                         get_info_call['user']['lastname'], get_info_call['user']['birthday'],
                         get_info_call['user']['sex'], get_info_call['user']['city'],
                         get_info_call['user']['state'], get_info_call['user']['country'],
                         get_info_call['user']['about'], get_info_call['user']['friends_count'],
                        get_info_call['user']['followers_count'], 0))
            con_db.commit()
            uid_list.append(uid)
        except sqlite3.IntegrityError:
            pass

        uid += 1
        i += 1


    # ************ END NODES **************


    # ************ LINK **************
    cur.execute('SELECT * FROM nodes WHERE is_crawled =0')
    nodes = cur.fetchall()
    for node in nodes:

        # if exist follower
        if node[11]>0:
            # Prova a chiamare i follower se va a buon fine riempi la tabella delle
            # relazioni altrimenti esci e vai al prossimo nodo
            try:
                url = 'https://api.500px.com/v1/users/' + str(node[0]) + '/followers/'
                r = requests.get(url, auth=con_api, timeout=7)
                #Solo se sei in 200
                if r.status_code // 100 == 2:
                    followers_call = json.loads(r.text, "utf-8")
                    p = followers_call['followers_pages']

                    link_list=[]
                    #for cp in range(1, p + 1):
                    for cp in range(1, p + 1):

                        print "[uid: %d] Call follower: Page %d of %s" % (node[0], cp, p)
                        #r = call(node[0], "get_follower", con_api, page=cp)
                        url = 'https://api.500px.com/v1/users/' + str(node[0]) + '/followers/?page=' + str(cp) + '&rpp=' + str(
                        MAX_ITEM_PAGE)
                        r = requests.get(url, auth=con_api, timeout=7)
                        r_json = json.loads(r.text, "utf-8")

                        link_list += [x['id'] for x in r_json['followers']]


                    link_list_pruned = list(set(link_list).intersection(uid_list))
                    for link in link_list_pruned:
                        try:
                            cur.execute("INSERT INTO link VALUES (?,?)",(link, node[0]))
                            con_db.commit()
                        except sqlite3.IntegrityError:
                            pass

            except ValueError:
                pass

        if node[10] > 0:
            # Prova a chiamare i following se va a buon fine riempi la tabella delle
            # relazioni altrimenti esci e vai al prossimo nodo
            try:
                url = 'https://api.500px.com/v1/users/' + str(node[0]) + '/friends/'
                r = requests.get(url, auth=con_api, timeout=7)
                # Solo se sei in 200
                if r.status_code // 100 == 2:
                    friends_call = json.loads(r.text, "utf-8")
                    p = friends_call['friends_pages']

                    link_list = []
                    for cp in range(1, p + 1):
                        print "[uid: %d] Call following: Page %d of %s" % (node[0], cp, p)
                        url = 'https://api.500px.com/v1/users/' + str(node[0]) + '/friends/?page=' + str(cp) + \
                              '&rpp=' + str(MAX_ITEM_PAGE)
                        r = requests.get(url, auth=con_api, timeout=7)
                        r_json = json.loads(r.text, "utf-8")
                        link_list += [x['id'] for x in r_json['friends']]

                    link_list_pruned = list(set(link_list).intersection(uid_list))
                    for link in link_list_pruned:
                        try:
                            cur.execute("INSERT INTO link VALUES (?,?)", (node[0],link))
                            con_db.commit()
                        except sqlite3.IntegrityError:
                            pass
            except ValueError:
                pass


        # Update current element as crawled
        cur.execute("UPDATE nodes SET is_crawled=? WHERE id=?", ("1", node[0]))
        con_db.commit()














# python -m cProfile euler048.py








"""
TODO Modificare get in e out in modo che prenda il count e le pagine direttamente dalla risposta della chiamata (count utente fallace)
TODO Modifica script in modo che generi automaticamente il csv
"""

#
